#!/bin/bash
set -e

BZIP_EXECUTABLE="${1}"
MAX_LENGTH=$(( 1024*1024 ))
TESTS=10

# TODO Trap state in case of a failure to be able to debug

function main {
  
    for TEST_INDEX in $(seq 1 ${TESTS})
    do
        echo -n .
        local LENGTH=$(randomLength)
        local DATA=$(randomData ${LENGTH})
        
        for SMALL_ARG in '' '--small'
        do
            for COMPRESSION in 1 2 3 4 5 6 7 8 9
            do                
                local GOT=$(echo ${DATA} | base64 -D | ${BZIP_EXECUTABLE} -z ${SMALL_ARG} -${COMPRESSION} | ${BZIP_EXECUTABLE} -d | base64)
                                
                if [[ ${DATA} != ${GOT} ]]
                then
                    echo "Test failed!"
                    echo "${BZIP_EXECUTABLE} ${SMALL_ARG} -${COMPRESSION}"
                    echo "Expected (${LENGTH} bytes):"
                    echo "${DATA}"
                    echo "Got:"
                    echo "${GOT}"
                    exit 1
                fi
            done        
        done
    done
    
    echo OK
}

function randomLength {
   jot -r 1 1 ${MAX_LENGTH}
}

function randomData {
    local LENGTH=$1
    head -c ${LENGTH} /dev/urandom | base64
}

main